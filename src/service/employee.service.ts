import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { IEmployee } from "../component/employee/employee.component";

@Injectable()
export class EmployeeService {

  constructor(private http: HttpClient) { }

  getAllEmployees(): Observable<IEmployee[]> {
    return this.http.get<IEmployee[]>("http://localhost:8080/api/employee/getAll")
      .pipe(
        tap(employees => console.info.bind("Employees obtained from the web service")),
        catchError(console.error.bind("problems getting employees"))
    );
  }

  getEmployee(id: string): Observable<IEmployee> {
    return this.http.get<IEmployee>("http://localhost:8080/api/employee/get/"+id)
      .pipe(
        tap(employee => console.info.bind("Employee obtained from the web service")),
        catchError(console.error.bind("problems getting employee"))
    );
  }
}
