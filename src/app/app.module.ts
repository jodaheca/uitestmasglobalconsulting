import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { EmployeeComponent } from '../component/employee/employee.component';
import {MatTableModule} from '@angular/material/table';
import { FormsModule }   from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import {EmployeeService } from '../service/employee.service';

@NgModule({
  declarations: [
    AppComponent,
    EmployeeComponent
  ],
  imports: [
    BrowserModule,
    MatTableModule,
    FormsModule,
    HttpClientModule

  ],
  providers: [HttpClient, EmployeeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
