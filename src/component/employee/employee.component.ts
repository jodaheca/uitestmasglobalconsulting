import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../../service/employee.service';

export interface IEmployee {
  id: string;
  name: string;
  contractTypeName: string;
  roleId: string;
  roleDescription: string;
  hourlySalary: number;
  monthlySalary: number;
  annualSalary: number;
}

const ELEMENT_DATA: IEmployee[] = [];

export class EmployeeForm {
  id: string;
}

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  displayedColumns: string[] = ['id', 'name', 'contractTypeName', 'roleId', 'roleDescription', 'hourlySalary', 'monthlySalary', 'annualSalary'];
  dataSource = ELEMENT_DATA;

  constructor( private employeeService: EmployeeService) { }

  ngOnInit() {}

  employee = new EmployeeForm();

  onSubmit(){
    this.dataSource = ELEMENT_DATA;
    if (typeof this.employee.id === 'undefined' || this.employee.id == "") {
      this.employeeService.getAllEmployees().subscribe(
        data => {
            if(data){
                  console.log(data);
                  this.dataSource = data;
            }
        },
        err => { console.error.bind("problems getting employee")}
      );
    } else{
      this.employeeService.getEmployee(this.employee.id).subscribe(
        data => {
            if(data){
                  console.log(data);
                  this.dataSource = [data];
            }
        },
        err => { console.error.bind("problems getting employee")}
      );
    }
}

}
